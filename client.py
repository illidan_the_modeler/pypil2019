from socket import socket
import sys 

def main():
    cli_sock=socket()
    if '-d' in sys.argv:
        addr='127.0.0.1'
        port=21
    else:
        addr = input('Please specify the server address.')
        port = int (input('Please specify the port No.'))
    cli_sock.connect((addr, port))
    while True:
        data_buf=cli_sock.recv(1024)
        print('Recved: %s' % data_buf.decode('UTF-8'))
        cli_sock.send('hELLO wORLD!\n'.encode('UTF-8'))
    cli_sock.close()
   

if __name__=='__main__':
    main()

