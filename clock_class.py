'''
Zhihu Tutorial
'''

from time import sleep
#from time import localtime
from os import system as sys

class Clock(object):
    def __init__(self):
        self._hour = 0
        self._minute = 0#
        self._second = 0

    def tick(self):
        while True:
            self._second += 1
            if self._second==60:
                self._second=0
                self._minute+=1
                if self._minute==60:
                    self._hour+=1
                    if self._hour==24:
                        self._hour=0
            print('%d:%d:%d' %(self._hour, self._minute, self._second))
            sleep(1)
            sys('clear')


def main():
    clock = Clock()
    clock.tick()

if __name__=='__main__':
    main()

