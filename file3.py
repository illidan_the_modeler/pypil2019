'''
Luo Hao tutorial Day 11. File open, read and write.
'''

from mymath import find_prime_number

def write_prime_numbers(f, low, high):
    primes=find_prime_number(low, high)
    str_primes=''
    for i, e in enumerate(primes):
        str_primes+=str(e)
        if i%20==0:
            str_primes+='\n'
        else:
            str_primes+=' '
    str_primes+='\n'
    f.write(str_primes)

def demo():
    f = open("a.txt", 'w', encoding='utf-8')
    f.write('It begins.\n')
    write_prime_numbers(f, 1, 100)
    write_prime_numbers(f, 100, 1000)
    f.close()

    try:
        with open("a.txt", "r", encoding='utf-8') as f:
            line=f.read()
            print(line)
            print('*******read***********')
        with open("a.txt", "r", encoding='utf-8') as f:
            for line in f:
                print(line, end='')
            print()
            print('*******for-in***********')
        with open("a.txt", "r", encoding='utf-8') as f:
            lines=f.readlines()
            print(lines)
            print('*******readlines***********')
    except FileNotExistError:
        print('File Not Found.')
    except LookUpError:
        print('encoding unkown.')
    except UnicodeDecodeError:
        print('unicode decode unkown.')
        #

if __name__=='__main__':
    demo()

