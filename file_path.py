'''
Luo Hao tutorial Day 11. File open, read and write.
'''

def demo():

    try:
        with open("/home/wjb/server_files/server.bz2", 'rb') as f :
            data_buff=f.read()
    except FileNotExistError:
        print('File Not Found.')
    except LookUpError:
        print('encoding unkown.')

    try:
        with open("/home/wjb/client_files/server.bz2", 'wb') as f :
            f.write(data_buff)
    except FileNotExistError:
        print('File Not Found.')

if __name__=='__main__':
    demo()

