from ftp_protocol import FTProtocolCli
import logging
import sys

class FTPCliUI(object):
    def __init__(self):
        if '-d' in sys.argv:
            self._ftp_proto_cli=FTProtocolCli(None)
        elif len(sys.argv)==2:
            self._ftp_proto_cli=FTProtocolCli(None, sys.argv[1])
        else:
            self._ftp_proto_cli=FTProtocolCli(None, input('(Please enter the server address.)\n Open: '))
    def proc_user_cmd(self, cmd):
        self.get_proto().proc_cmd(cmd)

    #def send_command(self, cmd):
    #    self._ftp_proto_cli.send_command(cmd)

    def get_proto(self):
        return self._ftp_proto_cli

    def proc_reply(self):
        response=self._ftp_proto_cli.proc_reply()
        self._reply=response
        logging.info('Received server reply: %s' % self._reply)
        print(self._reply)

def main():
    logging.basicConfig(filename='ftp_client.log', level=logging.DEBUG)
    ftp_cli_ui = FTPCliUI()
    ftp_cli_ui.proc_user_cmd('connect')
    while True:
        cmd=input('ftp: >')
        if cmd=='q' or cmd=='Q':
            break
        elif cmd=='pwd':
            ftp_cli_ui.proc_user_cmd(cmd)
            ftp_cli_ui.proc_reply()
        elif cmd=='exit' or cmd=='quit':
            #TODO: add code to close the socket
            exit()
        else:
            print('Unsupported command.')

if __name__=='__main__':
    main()

