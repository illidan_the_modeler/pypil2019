from socket import socket

def main():
    cli_sock=socket()
    addr = input('Please specify the server address.')
    port = int (input('Please specify the port No.'))
    cli_sock.connect((addr, port))
    data_buf=cli_sock.recv(1024)
    #print('Recved: %s' % data_buf.decode('UTF-8'))
    cli_sock.send('hELLO wORLD!\n'.encode('UTF-8'))
    cli_sock.close()
    try:
        with open('/home/wjb/client_files/client.bz2', 'wb') as f:
            f.write(data_buf)
    except FileNotExistError:
         print('File Not Found.')
    

if __name__=='__main__':
    main()

