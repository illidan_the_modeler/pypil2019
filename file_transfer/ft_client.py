from socket import socket

class FTClient(object):
    #
    def run(self):
        buf_size=4096
        cli_sock=socket()
        #addr = input('Please specify the server address.')
        #port = int (input('Please specify the port No.'))
        addr='127.0.0.1'
        port=2121
        data = bytes()
        cli_sock.connect((addr, port))
        data_buf=cli_sock.recv(buf_size)
        while data_buf:
            data += data_buf
            data_buf=cli_sock.recv(buf_size)
        print('Received %d bytes of data.' % len(data))
        cli_sock.close()
        try:
            with open('/home/wjb/pypil2019/file_transfer/result.jpg', 'wb') as f:
                f.write(data)
        except FileNotFoundError:
             print('File Not Found.')

def main():
    ft_client=FTClient()
    ft_client.run()

if __name__=='__main__':
    main()

