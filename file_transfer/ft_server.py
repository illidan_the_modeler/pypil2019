from socket import socket
from threading import Thread

class FileTransferHandler(Thread):
    def __init__(self, cclient, data):
        super().__init__()
        self.cclient = cclient
        self._data=data
    
    def run(self):
        self.cclient.send(self._data)
        self.cclient.close()


def main():
    #data=bytes()
    try:
        with open('/home/wjb/pypil2019/file_transfer/test/thread_sock.jpg', 'rb') as f:
            data=f.read()
    except FileNotFoundError:
        print('File Not Found.')
    server_sock=socket()
    server_sock.bind(('127.0.0.1', 2121))
    server_sock.listen()
    print('Server start listening.')
    while True:
        client_sock, addr=server_sock.accept()
        print('Connection inbound...')
        print('bytes sent is %d.' % (len(data)))
        FileTransferHandler(client_sock, data).start()

if __name__=='__main__':
    main()

