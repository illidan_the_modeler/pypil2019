
from socket import socket, AF_INET, SOCK_STREAM, \
        gethostbyname, gethostname
from threading import Thread
import os
import logging
from configparser import  ConfigParser
from random import randint

'''Should I add seperate threads for user input and socket recv?
'''
class FTPReply(object):
    def __init__(self, raw_reply):
        self._code=int(raw_reply[:3])
        self._msg=raw_reply[4:]

    @property
    def code(self):
        return self._code
    
    @code.setter
    def code(self, code):
        self._code=code

    @property
    def msg(self):
        return self._msg
    
    @msg.setter
    def msg(self, msg):
        self._msg=msg

    def to_str(self):
        return str(self._code)+self._msg

REPLY_MSG={\
        'CMD_UNSUPPORTED': 'Command is not supported. Please use legal commands.\n', \
        'Welcome': '100Welcome to the FTP  Server',\
                'MSG257': '257'\
        }

class FTProtocol(object):
    ''' PRotocol spec common to both the server and the client
    Common attributes:
    _sig_sock
    _port
    _addr
    _wd
    _buf_size
    '''
    def __init__(self, home, addr, socket):
        self._buf_size=4096
        self._port=2121
        self._addr=addr
        #self._data_port

        ''' TODO: HOME directory should also be configurable
        '''
        self._home=home
        self._wd=self._home
        self._sig_sock=socket
        self._data_sock=None

    def _get_socket(self):
        return self._sig_sock

    @property
    def wd(self):
        return self._wd

    @wd.setter
    def wd(self, path):
        self._wd=path

    def get_buf_size(self):
        return self._buf_size

class FTProtocolCli(FTProtocol):
    ''' A protocol client, and a user client agent
    '''
    def __init__(self, home=None, addr='127.0.0.1'):
        a_socket=socket(AF_INET, SOCK_STREAM)
        self._reply_arr=[]
        super().__init__(home, addr, a_socket)

    def proc_cmd(self, cmd):
        is_success=True
        if cmd=='connect':
            self._connect()
            #self.send_command('PWD')
        elif cmd=='pwd':
            self.send_command(cmd)
        else:
            logging.info('Unsupported command.')
            is_success=False
        return is_success

    def _connect(self):
        logging.debug('Connecting to %s:%d...' % (self._addr, self._port))
        print('Connecting to %s:%d...' % (self._addr, self._port))
        self._sig_sock.connect((self._addr, self._port))

    def send_command(self, cmd):
        if not self._sig_sock:
            self._connect()
        self._get_socket().send(cmd.encode('utf-8'))

    def proc_reply(self):
        data_buf=self._sig_sock.recv(self._buf_size)
        raw_reply=data_buf.decode('utf-8')
        logging.debug('Received reply: %s' % raw_reply)
        reply = FTPReply(raw_reply)
        reply.success=True
        if reply.code=='':
            self._open_data_chan()
        elif reply.code==257:
            logging.info('Command executed successfully.')
        elif reply.code==257:
            logging.info('Command executed successfully.')
        else:
            reply.success=True
            logging.info('Server reply not supported.')

        self._reply_arr.append(reply)

        return raw_reply

    def _open_data_chan(self):
        self._data_sock=socket(AF_INET, SOCK_STREAM)
        self._data_sock.connect((server_addr, server_port))

class FTProtocolServer(FTProtocol):
    def __init__(self, socket, addr, ueid, server, config_file_path=None):
        self._config=ConfigParser()
        if not config_file_path:
            logging.DEBUG('Config file is not found.')
        else:
            self._config.read(config_file_path)
        home=self._config['FTP Attributes']['home']
        if not home:
            home=os.getenv('HOME')
        self._sig_sock=socket
        self._server=server
        self._ueid=ueid
        super().__init__(home, addr, socket)
    #def __init__(self, home, addr, socket):

    def get_config(self):
        return self._config

    def get_sock(self):
        return self._sig_sock

    def get_bcpuf_size(self):
        return self._buf_size

    def send_reply(self, reply):
        logging.debug('Sent reply: %s' % reply)
        self._sig_sock.send(reply.encode('utf-8'))

    def _proc_commands(self, cmd):
        logging.debug('Received command: %s' % cmd)
        if cmd=='CWD' or cmd=='cwd':
            reply = REPLY_MSG['MSG257']+self._home
            self.send_reply(reply)
        elif cmd=='PWD' or cmd=='pwd':
            self._proc_pwd()
        else:
            reply = REPLY_MSG['CMD_UNSUPPORTED']

    def _proc_pasv(self, cmd):
        self._data_port=randint(\
                self._ueid, self._config['FTP Attributes']['max_pasv_port'])
        reply = REPLY_MSG['MSG257']+str(self._data_port)
        self.send_reply(reply)

    def _proc_pwd(self):
        reply = REPLY_MSG['MSG257']+self._home
        self.send_reply(reply)

    def run(self):
        cmd_buf = self.get_sock().recv(self.get_buf_size())
        cmd=cmd_buf.decode('utf-8')
        logging.debug('Received command %s' % cmd)
        if cmd:
            self._proc_commands(cmd)

class FTPSvrHdlr(Thread):
    def __init__(self, sig_sock, addr, ue_id, server):
        '''TODO: make buffer size configurable
        '''
        super().__init__()
        self._ftp_proto_svr = FTProtocolServer(\
                sig_sock, addr, ue_id, server, 'ftp_server.ini')
        welcome=FTPReply(REPLY_MSG['Welcome'])
        logging.debug(str(welcome))
        self._ftp_proto_svr.send_reply(welcome.to_str())

    def run(self):
        self._ftp_proto_svr.run()

class FTPSvrListener(object):
    #def __init__(self, addr):
    def __init__(self):
        ''' TODO: Addr and port configurable
            TODO: How to add attributes like a dict for a class
        '''
        self._create_listen_sock()
        self._cli_handles=[]
        self._ue_counter=0

    def _create_listen_sock(self):
        self._listen_sock=socket(AF_INET, SOCK_STREAM)
        self._addr=gethostbyname(gethostname())
        '''
        if '127.0.0.1'==addr:
            self._addr='192.168.2.162'
        else:
            self._addr=addr
        '''

        self._port=2121
        self._listen_sock.bind((self._addr, self._port))

    def start(self):
        self._listen_sock.listen()
        print('Listening on addr %s.' % self._addr)
        while True:
            sig_sock, addr = self._listen_sock.accept()
            self._ue_counter+=1
            cli_handle=FTPSvrHdlr(\
                    sig_sock, self._addr, self._ue_counter, self)
            self._cli_handles.append(cli_handle)
            cli_handle.start()
        #
