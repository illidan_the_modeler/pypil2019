from socket import socket


def main():
    data_buf=None
    try:
        with open('/home/wjb/server_files/server.bz2', 'rb') as f:
            data_buf=f.read()
    except FileNotExistError:
        print('File Not Found.')

    server_sock = socket()
    server_sock.bind(('127.0.0.1', 2121))
    print('Begin listening...')
    server_sock.listen()
    client_sock, addr=server_sock.accept()
    client_sock.send(data_buf)
    data_buf=client_sock.recv(4096)

    print('Received from %s:  %s' \
        % (addr, data_buf.decode('UTF-8')))

if __name__=='__main__':
    main()

