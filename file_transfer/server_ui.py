from  ftp_protocol import FTPSvrListener
import logging

if __name__=='__main__':
    logging.basicConfig(filename='ftp_server.log', level=logging.DEBUG, \
            filemode='w', format='%(message)s')
    ftp_server=FTPSvrListener()
    ftp_server.start()

