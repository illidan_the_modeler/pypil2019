'''
'''

def gcd(op1, op2):
    upbound = min (op1, op2)
    for div in range(1, upbound):
        if op1%div==0 and op2%div==0:
            key = div
    return key

if __name__=='__main__':
    op1 = int(input('Please input the numbers: '))
    op2 = int(input(' '))
    gcd = gcd(op1, op2)
    print('gcd of %d and %d = %d' % (op1, op2, gcd))
    

