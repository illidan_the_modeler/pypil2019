'''
Zhihu Tutorial Day 5: Lily numbers. Lily numbers are numbers each of which equals the sum of cubic of each digit.
'''

def lily_num_finder(upbound):
    if upbound>1000 or upbound<=0:
        print('We do not accept the range you have specified.')
        return
    lily_numbers=[]
    for i in range(100, upbound):
        a = i//100
        b = i//10%10
        c = i%10
        if a**3+b**3+c**3==i:
            lily_numbers.append(i)
    return lily_numbers

if __name__=='__main__':
    lily_numbers=lily_num_finder(1000)
    print(lily_numbers)

