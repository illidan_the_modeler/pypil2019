'''
Zhihu-Luo Hao tutorial Day 11: read-write file in binary mode.
'''
from sys import argv

def mycopy(src_file_path, dest_file_path):
    try: 
        with open(src_file_path, "rb") as src_file:
            src_file_bytes=src_file.read()
    except FileNotExistError:
        print("File does not exist.")
    try:
        with open(dest_file_path, "wb") as dest_file:
            dest_file.write(src_file_bytes)
    except IOError:
        print("IO error.")

def main():
    if len(argv)!=3:
        print('Usage: mycopy <src_file> <dest_file>')
    else:
        mycopy(argv[1], argv[2])#

if __name__=='__main__':
    main()

