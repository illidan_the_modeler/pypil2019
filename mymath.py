'''
A number of methods to calculate the square root of a real number.
'''

ERROR_LIMIT=0.000001
MAX_TRIES=100 
NUM_DIGITS=6


def abs(x):
    if x<0:
        return x*(-1)
    else:
        return x

def power(base, index):
    power = 1
    for i in range(index):
        power*=base
    return power

def sci_rep(x):
    coeff=float(x)
    for index in range(MAX_TRIES):
        if coeff>=1 and 100>coeff:
            break
        coeff=coeff/100
    sci=(coeff, index)
    return sci

def sqrt2(x):
    (coeff, index)= sci_rep(x)
    remainder=coeff
    for b in range(10):
        if b*b>coeff:
            a=b-1
            remainder-=a*a
            break
    for i in range(NUM_DIGITS+1):
        a*=10
        remainder*=100
        for b in range(10):
            if b*(2*a+b)>remainder:
               remainder-=(2*a+b-1)*(b-1)
               a+=b-1
               break
    a//=10
    if b-1>=5:
        a+=1
    return float(a/power(10, NUM_DIGITS))*power(10, index)

def sqrt1(operand):
#newton_method
    x0 = 1.0
    for i in range(MAX_TRIES):
        x1=x0-(x0*x0-operand)/(2*x0)
        if abs(x1-x0)<ERROR_LIMIT:
            return x1
            break
        else:
            x0=x1

def sqrt3(operand):
    if operand>4.0:
        low,high = 2.0, (operand/2)
    else:
        low,high = (operand/2), 2.0
    for i in range(MAX_TRIES):
        root = (low+high)/2
        if ERROR_LIMIT>abs(operand-root*root):
                break
        print("Cal count %d. root = %f" % (i, root))
        if root*root>operand:
            high = root
        else:
            low = root
    return root

def find_prime_number(lowbound, upbound):
    prime_numbers=[]
    for i in range(lowbound, upbound+1):
        is_prime=True
        if i<=1:
            continue
        elif i<=4:
            search_upbound=i
        else:
            search_upbound=int(sqrt1(i))+1
        for j in range(2, search_upbound):
            if i%j==0:
                is_prime=False
                break
        if is_prime:
            prime_numbers.append(i)
    return prime_numbers


if __name__=='__main__':
    operand=float(input('Please input the number you\'d like to caculate the square root for: '))
    root=sqrt1(operand)
    print('The sqaure root of %.3f is %.6f\n' %(operand, root))
    root=sqrt2(operand)
    print('The sqaure root of %.3f is %.6f\n' %(operand, root))
    root=sqrt3(operand)
    print('The sqaure root of %.3f is %.6f\n' %(operand, root))
    print(find_prime_number(1, 100))

