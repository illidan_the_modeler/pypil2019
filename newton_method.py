''' 
To solve equation: f(x)=0.
Using Taylor's expansion formula f(x)=f(x0)+f'(x0)*(x-x0)k
We have x=x0-f(x0)/f'(x0)
Chaning form into an iteration:  x[n+1]=x[n]-f(x[n])/f'(x[n])
'''
from sys import  argv

epsilon=0.000001
#recursion_counter=0
max_recursion_times=100

def abs(x):
    if x<0:
        return -x
    else:
        return x

def poly_calc(x,coeff):
    sum=0
    for i,a in enumerate(coeff):
        sum=sum*x+a
    return sum

def derivative(coeff):
    rank=len(coeff)-1
    derivative_coeff=[]

    for i in range(rank):
        derivative_coeff.append((rank-i)*coeff[i])
    return derivative_coeff

def newton_method1(coeff):
    #coeff=[1,0,-input_op]
    x0=1.0
    derivative_coeff=derivative(coeff)
    for i in range(10000):
        x1=x0-poly_calc(x0,coeff)/poly_calc(x0, derivative_coeff)
        if abs(x1-x0)<epsilon:
            return x1
            break
        else:
            x0=x1

      
def newton_method2(x0,coeff, recursion_counter):
    derivative_coeff=derivative(coeff)
    x1=x0-poly_calc(x0,coeff)/poly_calc(x0, derivative_coeff)
    if abs(x1-x0)<epsilon:
        return x1
    else:
        recursion_counter += 1
        if recursion_counter>=max_recursion_times:
            return None
        else:
            return newton_method2(x1,coeff, recursion_counter)
      

if __name__=='__main__':
    #coeff=[1,-3,1]
    if len(argv)==1: #Calculating square root
        coeff=[1.0,0.0]
        coeff.append(-1.0*float(input('Please input the number you want to calculate square root: ')))
    elif len(argv)>1:
        coeff=[]
        for i,a in enumerate(argv[i]):
            if i>0:
                coeff.append(a)

    root=newton_method1(coeff)
    if root==None:
        print('The equation does not have a root in the domain of real number.')
    else:
        print('The key given by method1 is %.6f' % root)

    root=newton_method2(2.0, coeff, 0)
    if root==None:
        print('The equation does not have a root in the domain of real number.')
    else:
        print('The key given by method2 is %.6f' % root)

