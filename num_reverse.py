
from math import log10

def number_reverser(number):
    num_array = []
    reversed_num  = 0
    rank = int(log10(number))
    for i in range(rank+1):
        digit = number // (10**i) % 10
        num_array.append(digit)
    for i,digit in enumerate(num_array):
        reversed_num += digit*(10**(rank-i))
    return reversed_num
    #

if __name__=='__main__':
    number = int(input('Input the integer number you want to reverse: '))
    reversed_num = number_reverser(number)
    print('\nOriginal number is %d. And the reversed number is %d' % (number, reversed_num))

