'''
Zhihu tutorial Day 4
'''

import random

key = random.randint(1, 100)
total_times=0

for counter in range(101):
    guess = int(input('Please input your guess: '))
    if guess > key:
        print('Your guess is too large.')
    elif guess < key:
        print('Your guess is too small.')
    else:
        print('Bingo. The key is %d.' % key)
        total_times=counter-1
        break

print('You have tried %d times.' % total_times)
    
