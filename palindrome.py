'''
Zhihu tutorial Day 5 'Reversing digits of a number'
'''

from math import log10

def is_palindrome(number):
    sum = 0
    remain = number
    while remain > 0 :
        sum = sum*10 + remain % 10
        remain = remain // 10
    return (sum==number)
    
def is_palindrome2(number):
    num_array = []
    reversed_num  = 0
    rank = int(log10(number))
    for i in range(rank+1):
        digit = number // (10**i) % 10
        num_array.append(digit)
    for i,digit in enumerate(num_array):
        reversed_num += digit*(10**(rank-i))
    return number==reversed_num
 
if __name__=='__main__':
    number = int(input('Input the integer number to test: '))
    if is_palindrome(number):
        print('\n%d is a palimdrome.\n' % (number))
    else:
        print('\n%d is not a palimdrome.\n' % (number))

    if is_palindrome2(number):
        print('\n%d is a palimdrome.\n' % (number))
    else:
        print('\n%d is not a palimdrome.\n' % (number))
