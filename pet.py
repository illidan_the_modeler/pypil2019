'''
Zhihu Tutorial: Day 9 OOP "abstract class".

Summary: In a sub-class, the abstract method defined in super class may be or may be not implemented.
'''

from abc import ABCMeta, abstractmethod

class Pet(object, metaclass=ABCMeta):
    def __init__(self, name):
        self._name = name

    @abstractmethod
    def make_sound(self):
        pass

class Dog(Pet):
    def make_sound(self):
        print('%s woof...\n' % (self._name))

class Cat(Pet):
    def __init__(self, name):
        self._name=name
    def make_sound(self):
        print('%s meow...\n' % (self._name))

def main():
    pets = [Dog('bionic'), Cat('xenial')]
    for pet in pets:
        pet.make_sound()

if __name__=='__main__':
    main()

