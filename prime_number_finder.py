from math import sqrt

def prime_number_finder(lowbound, upbound):
    prime_numbers=[]
    for i in range(lowbound, upbound+1):
        is_prime=True
        if i<=1:
            continue
        elif i<=4:
            search_upbound=i
        else:
            search_upbound=int(sqrt(i))+1
        for j in range(2, search_upbound):
            if i%j==0:
                is_prime=False
                break
        if is_prime:
            prime_numbers.append(i)
    return prime_numbers


if __name__=='__main__':
    print(prime_number_finder(1, 100))
    print(prime_number_finder(5, 100))
    print(prime_number_finder(5, 5))

