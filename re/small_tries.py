
import re
import sys

class REPractice(object):
    def username_qq_no(self):
        '''
        #username can be made of letters and numbers
        #at least 5 character
        #at most 20 charcters

        # QQ number is a number made of 5 to 11 digits
        # 0 cannot be the first digit
        '''
        if '-d' in sys.argv:
            username='weijiangbei'
            qq_no='19158873'
        else:
            username = input('Please input your username: ')
            qq_no = input('Please input your QQ number: ')

        #matched: re1=re.match(r'^[a-z]$', username)
        #matched: re1=re.match(r'^[a-zA-Z0-9]{5, 20}$', username)
        if not re.match(r'^[a-zA-Z0-9]{5,20}$', username):
            print('The username is illegal.')
        else:
            print('The username is accepted.')

        if not re.match(r'^[1-9][0-9]{4,10}$', qq_no):
            print('The QQ Number is illegal.')
        else:
            print('The QQ number is accepted.')

def main():
    p1=REPractice ()
    p1.username_qq_no()

if __name__=='__main__':
    main()





