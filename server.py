from socket import socket


def main():
    server_sock = socket()
    port = 21
    #with server_socket() as server_sock:
    server_sock.bind(('127.0.0.1', port))
    print('Begin listening on port %d ...' % port)
    server_sock.listen()
    while True:
        client_sock, addr=server_sock.accept()
        client_sock.send('Hello World!\n'.encode('UTF-8'))
        data_buf=client_sock.recv(4096)

    server_sock.close()
    print('Received from %s:  %s' \
            % (addr, data_buf.decode('UTF-8')))

if __name__=='__main__':
    main()

