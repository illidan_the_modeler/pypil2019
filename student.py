'''
Zhihu Toturial Day 9: Advanced OOP: regarding setter
'''

class Student(object):
    def __init__(self):
        self._grade=0

    @property
    def score(self):
        return self._grade

    @score.setter
    def score(self, score):
        if score<0 or score>100:
            raise ValueError('Invalid score')
        self._grade=score

def main():
    stu = Student()
    stu.score = 59
    print(stu.score)
    stu.score = 101
    print(stu.score)

if __name__=='__main__':
    main()





