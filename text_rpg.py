'''
Zhihu Tutorial Day 9: advanced OOP.
'''

from abc import ABCMeta, abstractmethod
from random import randint, randrange


class Character(object, metaclass=ABCMeta):
    def __init__(self, name, hp, mp):
        self._name=name
        self._hp=hp
        self._mp=mp
        self._power=0

    def get_name(self):
        return self._name

    @property
    def hp(self):
        return self._hp

    @hp.setter
    def hp(self, hp):
        self._hp=hp if hp>0 else 0

    def recv_damage(self, damage):
        if self._hp<damage:
            real_damage=self._hp
            self._hp=0
        else:
            real_damage=damage
            self._hp-=damage
        return real_damage

    @property
    def mp(self):
        return self._mp

    @hp.setter
    def mp(self, hp):
        self._hp-=hp
        if self._hp<0:
            self._hp=0

    def is_alive(self):
        if self._hp>0:
            return True
        else:
            return False

    def phy_attack(self, foe):
        if foe.is_alive()==False:
            return False
        damage=foe.recv_damage(self.get_power())
        print('%s uses physical attack against %s, dealing damage of %d. HP of %s is now %d.' % \
                (self._name, foe.get_name(), damage, foe.get_name(), foe.hp))
        if foe.hp==0:
            print("######%s has been slain by %s." % (foe.get_name(), self._name))
        return True

 

class Hero(Character):

    '''
    @property
    def power(self):
        return self._power

    @power.setter
    def power(self):
        self._power=randint(15,25)
    '''

    def get_power(self):
        return self._power

    def update_power(self):
        self._power=randint(150,250)

    def magic_attack(self, monsters):
        if self._mp<20:
            return False
        self._mp-=20
        for monster in monsters:
            if monster.is_alive()==False:
                continue
            damage=monster.recv_damage(self.get_power())
            print('%s uses magic attack against %s, dealing damage of %d. HP of %s is now %d.' \
                    % (self._name, monster.get_name(), damage, monster.get_name(), monster.hp))
            if monster.hp==0:
                print("######%s has been slain by %s." % (monster.get_name(), self._name))
        return True

    def super_attack(self, monster):
        if self._mp<50:
            self.phy_attack(monster)
            return False
        self._mp-=50
        damage=monster.hp*3//4
        damage=damage if damage>50 else 50
        monster.recv_damage(damage)
        print('>>>>>>>>%s uses super attack against %s, dealing damage of %d. HP of %s is now %d.' \
                % (self._name, monster.get_name(), damage, monster.get_name(), monster.hp))
        if monster.hp==0:
            print("######%s has been slain by %s." % (monster.get_name(), self._name))
        return True

    def recover_mp(self):
        self._mp+=15
        print('%s has an MP of %d' % (self._name, self._mp))

class Monster(Character):
    '''
    @property
    def power(self):
        print("get called")
        return self._power

    @power.setter
    def power(self):
        print("set called")
        self._power=randint(15,25)

    '''

    def get_power(self):
        return self._power

    def update_power(self):
        self._power=randint(5,10)

    def is_any_alive(monsters):
        for monster in monsters:
            if monster.is_alive()==True:
                return True
        return False

def demo():
    hero = Hero('Tux', 500, 100)
    monsters=[Monster('Diablo', 1000, 200), Monster('Baal', 1500, 300), Monster('Mephisto', 800, 150)]

    for rounds in range(1, 11):
        print('\nRound %d:' % rounds)
        hero.update_power()
        for monster in monsters:
            monster.update_power()
        target_monster=monsters[randint(0,len(monsters)-1)]
        rand = randint(1,10)
        if rand<=6:
            hero.phy_attack(target_monster)
        elif rand<=9:
            hero.magic_attack(monsters)
        else:
            hero.super_attack(target_monster)
        for monster in monsters:
            if monster.is_alive()==False:
                print(monster.get_name())
                monsters.remove(monster)

        #TODO: When Diablo is slain, Baal does not attack.
        for monster in monsters:
            monster.phy_attack(hero)
            if hero.is_alive()==False:
                print('\n******Game Over******')
                return False
        hero.recover_mp()
    print('\n******Game Over******')
    return True

if __name__=='__main__':
    demo()

