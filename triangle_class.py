'''
Zhihu Tutorial Day 9: Advanced topic for OOP.
'''

class Triangle(object):
    def __init__(self, a, b, c):
        self._a = a
        self._b = b
        self._c = c

    def is_triangle(a, b, c):
        if a+b<=c or b+c<=a or c+a<=b:
            return False
        return True

    def cal_area(self):
        delimiter = float(self._a+self._b+self._c)
        return (delimiter/2*(delimiter/2-self._a)*(delimiter/2-self._b)*(delimiter/2-self._c))**0.5

def main():
    a,b,c = 5, 3, 4
    if Triangle.is_triangle(a, b ,c)==True:
        triangle = Triangle(a, b, c)
        print('\nThe triangle\'s edges are %d, %d, %d. Its area is %d\n' % (triangle._a, triangle._b, triangle._c, triangle.cal_area()))
    else:
        print('The edge lengths do not form a triangle.\n')

if __name__=='__main__':
    main()

